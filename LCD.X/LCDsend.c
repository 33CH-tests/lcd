/*! \file  LCDsend.c
 *
 *  \brief Send a byte to the LCD
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:47 AM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "LCDinternal.h"

//! LCDsend - Send a byte to the LCD
/*! Sends 8 bits of data to the LCD, pulses the LCD enable
 * bit to strobe the data in.
 *
 * Note that the data comes in as a byte, but the data lines
 * for the LCD are the high 4 bits of the port.  To avoid setting
 * individual bits one at a time, the data is shifted into the
 * appropriate place in the word, other bits are masked off, and
 * the high 4 bits of the port are masked off, then the two are
 * ORed together.
 *
 * \callgraph
 *
\dot
Digraph LCDsend
{
	
	node [ shape="record" fontname="Liberation Mono" ];
	in [ label="{Data|{15|14|13|12|11|10|09|08|<f7> 07|<f6> 06|<f5> 05|<f4> 04|<f3> 03|<f2> 02|<f1> 01|<f0> 00}}" ];
	out [ label="{{<f15> 15|<f14> 14|<f13> 13|<f12> 12|11|10|09|08|07|06|05|04|03|02|01|00}|Port}" ];
	in:f7 -> out:f15;
	in:f6 -> out:f14;
	in:f5 -> out:f13;
	in:f4 -> out:f12;
}
\enddot
\dot
Digraph LCDsend
{

	fontname="Helvetica-Bold"; fontsize=24.0;
	label="\nData to port mapping";
	node [ shape="record" fontname="Liberation Mono" ];
	in2 [ label="{Data|{15|14|13|12|11|10|09|08|<f7> 07|<f6> 06|<f5> 05|<f4> 04|<f3> 03|<f2> 02|<f1> 01|<f0> 00}}" ];
	out2 [ label="{{<f15> 15|<f14> 14|<f13> 13|<f12> 12|11|10|09|08|07|06|05|04|03|02|01|00}|Port}" ];
	in2:f3 -> out2:f15;
	in2:f2 -> out2:f14;
	in2:f1 -> out2:f13;
	in2:f0 -> out2:f12;
}
\enddot
 *
 * Pseudocode:
 * \code
 * Shift the data 8 bits to the left and mask all except the top 4 bits
 * Clear the top 4 bits of the LCD data port
 * OR the masked data with the LCD data port
 * LCDpulseEnableBit()
 * Shift the data 12 bits to the left and mask all except the top 4 bits
 * Clear the top 4 bits of the LCD data port
 * OR the masked data with the LCD data port
 * LCDpulseEnableBit()
 * \endcode
 *
 * \callgraph
 * \callergraph
 * 
 * \param data char - byte to be sent
 * \return none
 */
void LCDsend( char cdata )
{
  unsigned int data,adata;

  data = (unsigned int) cdata;

  adata = ( data << 8 ) & 0xF000;
  LCD_DATA &= 0x0FFF;
  LCD_DATA |= adata;
  LCDpulseEnableBit();

  adata = ( data << 12 ) & 0xF000;
  LCD_DATA &= 0x0FFF;
  LCD_DATA |= adata;
  LCDpulseEnableBit();
}

