/*! \file LCD.h
 *
 *  \brief LCD external definitions
 *
 * This file contains manifest constants, function prototypes, and macros
 * for users of the LCD library.
 *
 *  \author jjmcd
 *  \date January 3, 2015, 11:17 AM
 */
/* Software License Agreement
 * Copyright (c) 2015-2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

/***********************************************************************
 * NOTE
 * ****
 *
 * For delay to work properly, FCY must be defined before LCD.h is
 * included, unless the default (for us) 70 MHz clock is selected.
 *
 **********************************************************************/

#ifndef FCY
#define FCY 80000000
#endif

void __delay32( unsigned long );

#define __delay_ms(d) \
  { __delay32( (unsigned long) (((unsigned long long) d)*(FCY)/1000ULL)); }
#define __delay_us(d) \
  { __delay32( (unsigned long) (((unsigned long long) d)*(FCY)/1000000ULL)); }


/******	LCD FUNCTION PROTOYPES ******/

//! Initialize the LCD
void LCDinit( void );
//! Initialize the OLED
void OLEDinit( void );
//! Send a command to the LCD
void LCDcommand( char cmd );
//! Send a character to the LCD
void LCDletter( char data );
//! Send a string to the LCD
void LCDputs( char * );
//! Delay for a specified number of milliseconds
void Delay_ms( unsigned int );


/*****	LCD COMMAND MACROS  *****/

//! Move the LCD cursor to the right
#define LCDright()      LCDcommand( 0x14 )
//! Move the LCD cursor to the left
#define LCDleft()       LCDcommand( 0x10 )
//! Shift the LCD display
#define LCDshift()      LCDcommand( 0x1C )
//! Shift the LCD display left
#define LCDshiftLeft()      LCDcommand( 0x18 )
//! Shift the LCD display right
#define LCDshiftRight()      LCDcommand( 0x1c )
//! Clear the LCD display and home cursor
#define LCDclear()     { LCDcommand( 0x01 ); LCDcommand( 0x02 ); }
//! Set the LCD cursor to home
#define LCDhome()       LCDcommand( 0x02 )
//! Position the LCD cursor to the second line
#define LCDline2()      LCDcommand( 0xC0 )
//! Set the LCD cursor position
#define LCDposition(a)  LCDcommand( ((0x80 + ( (a) & 0x7f))) )
//! Turn the cursor on
#define LCDcursorOn() LCDcommand( (0x0e) )
//! Turn the cursor off
#define LCDcursorOff() LCDcommand( (0x0c) )
//! Make the LCD cursor position blink
#define LCDblinkOn() LCDcommand( (0x0d) )
//! Make the LCD cursor position blink AND show the cursor
#define LCDblinkAndCursor() LCDcommand( (0x0f) )
