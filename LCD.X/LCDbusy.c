/*! \file  LCDbusy.c
 *
 *  \brief Wait for the LCD to complete
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:49 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "LCDinternal.h"
#include "LCD.h"

//! Wait while LCD is busy
/*!  Dummied up with a 1ms delay
 *
 * \callergraph
 *
 * \par none
 * \returns none
 *
 * \todo Should go ahead and use R/W bit, although on the dsPIC-EL
 * board, the R/W pin is not connected.
 */
void LCDbusy( void )
{
    Delay_ms(DELAY_VALUE_15);
}

