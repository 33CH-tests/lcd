/*! \file  LCDinit.c
 *
 *  \brief Initialize the LCD
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:55 AM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "LCDinternal.h"
#include "LCD.h"

#define OLED

//! Initialize the LCD
/*!  LCDinit() first delays 15ms to allow the LCD internals to finish
 *   responding to power on.  This is not always necessary, but typically
 *   only happens once in a program.  The LCD initialization sequence is
 *   then sent, setting the LCD to  bit data.
 *
 *   LCD options are then sent, LCD 2 line, 5x7 font,  entry mode to
 *   not shift, display on, cursor off.
 *
 * Pseudocode:
 * \code
 * wait 15 ms
 * Establish state of all output pins
 * Set all output pins to be outputs
 * Send a 0x33 command
 * wait 5ms
 * Send a 0x32 command
 * wait 5 ms
 * Send a function set command (0x28)
 * Send display on, cursor off, bink off command (0x0c)
 * Send entry mode command (0x06)
 * \endcode
 * \callgraph
 *
 * \param none
 * \return none
 */
#ifdef OLED
void OLEDinit( void)
#else
void LCDinit( void)
#endif
{
    /* set initial states for the data and control pins */
    LCD_DATA &= 0x0FFF;
    LCD_RS = 0; // RS state set low
    LCD_ENABLE = 0; // E state set low

    /* set data and control pins to outputs */
    LCD_DATATRIS &= 0x0FFF;
    LCD_RS_TRIS = 0; // RS pin set as output
    LCD_ENABLE_TRIS = 0; // E pin set as output

    // 15mS delay after Vdd reaches nnVdc before proceeding with LCD initialization
    // not always required and is based on system Vdd rise rate
    Delay_ms(DELAY_VALUE_15);

    /* three 3's on the high four bits signals a mode command is coming.
     * the final 2 indicates four bit mode */
    /* 1st LCD initialization sequence */
    LCDcommand(0x33);
    Delay_ms(DELAY_VALUE_5);

    /* 2nd LCD initialization sequence */
    LCDcommand(0x32);
    Delay_ms(DELAY_VALUE_5);

    // Establish the LCD options
#ifdef OLED
    /* This sequence seems to be required for NHD-0216KZW-AB5 */
  LCDcommand(0x08); /* Display off */
  LCDcommand(0x28); /* LCD Function Set */
  LCDcommand(0x08); /* Display off */
  LCDcommand(0x01); /* Display clear */
  LCDcommand(0x06); /* Entry mode set */
  LCDcommand(0x02); /* Home */
  LCDcommand(0x0c); /* Display on */
#else
  LCDcommand(0x28); /* LCD Function set */
  LCDcommand(0x0c); /* Display on/off control, cursor blink off */
  LCDcommand(0x06); /* Entry mode set */
#endif
}
