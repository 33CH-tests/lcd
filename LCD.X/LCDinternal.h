/*! \file  LCDinternal.h
 *
 *  \brief Internal manifest constants for the LCD routines
 *
 * This file contains the manifest constants and function prototypes for
 * the LCD routines that are not required by external callers.  These
 * define the various pins and ports to which the LCD is connected.
 *
 * Note that changes to the LCD wiring will require not only changes to
 * these definitions, but also potential changes to the masking in
 * LCDsend() and LCDinit().
 *
 *  \author jjmcd
 *  \date January 3, 2015, 11:20 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef LCDINTERNAL_H
#define	LCDINTERNAL_H

#ifdef	__cplusplus
extern "C"
{
#endif

// Control signal data pins
// NOTE - LCD R/W pulled low
//! LCD Register select pin
#define  LCD_RS         LATBbits.LATB10
//! LCD Enable pin
#define  LCD_ENABLE     LATBbits.LATB11

// Control signal pin direction
//! LCD Register select direction register bit
#define  LCD_RS_TRIS        TRISBbits.TRISB10
//! LCD Enable direction register bit
#define  LCD_ENABLE_TRIS    TRISBbits.TRISB11

// Data signals and pin direction
//! LCD data port latch
#define  LCD_DATA      LATB
//! LCD data port
#define  LCD_DATAPORT  PORTB
//! LCD data port direction register
#define  LCD_DATATRIS  TRISB

#define DELAY_VALUE_1 1
#define DELAY_VALUE_5 5
#define DELAY_VALUE_15 15
#ifndef FCY
#define FCY 90000000
#endif

// Prototypes for functions internal to the LCD library
//! Toggle the LCD enable bit
void LCDpulseEnableBit( void );
//! Send a byte to the LCD
void LCDsend( char );
//! Wait while LCD is busy
void LCDbusy( void );


#ifdef	__cplusplus
}
#endif

#endif	/* LCDINTERNAL_H */

