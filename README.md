## LCD library for the dsPIC33CHxxxMP506 on the dsPIC-EL-CH

This library provides capability to manipulate the LCD on the
dsPIC-EL-CH. This LCD is a common 16 character by 2 line LCD
character display module.

Implements the following functions:

* `LCDinit()` - Initialize the LCD
* `LCDcommand()` - Send a command byte to the LCD
* `LCDletter()` - Send a data byte to the LCD
* `LCDputs()` - Send a string to the LCD
* `Delay_ms()` - Delay for a specified number of milliseconds
* `__delay32` - Delay a specified number of cycles

And the following macros:

* `LCDright()` - Move the cursor to the right one character
* `LCDleft()` - Move the cursor to the left one character
* `LCDshift()` - Shift the LCD memory to the right relative to the display
* `LCDshiftLeft()` - Shift the LCD memory to the left relative to the display
* `LCDshiftRight()` - Shift the LCD memory to the right relative to the display
* `LCDclear()` - Clear the LCD memory
* `LCDhome()` - Set the LCD cursor to address 0
* `LCDline2()` - Set the LCD cursor to address 0x40
* `LCDposition()` - Position the LCD cursor
* `LCDcursorOn()` - Make the LCD cursor visible
* `LCDcursorOff()` - Make the LCD cursor invisible
* `LCDblinkOn()` - Blink the character under the cursor
* `LCDblinkAndCursor()` - Blink and show the cursor
* `__delay_ms` - Delay a specified number of milliseconds
* `__delay_us` - Delay a specified number of microseconds

----

The header file, `LCD.h` only exposes the 5 primary functions shown 
above.  All the other LCD capabilities are provided through macros,
mostly by calls to `LCDcommand()`.  Three other functions are used
by the library, but not exposed through `LCD.h`.  They are, however,
prototyped in `LCDinternal.h`.

If a processor clock different than 80 MIPS is desired, the user
should specify `FCY` before including `LCD.h`,

The relationships between the functions
in the library is shown in the figure below:

![Functions](images/Functions.png)


----


