#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib.X.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib.X.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS
SUB_IMAGE_ADDRESS_COMMAND=--image-address $(SUB_IMAGE_ADDRESS)
else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../LCD.X/Delay_ms.c ../LCD.X/LCDsend.c ../LCD.X/LCDbusy.c ../LCD.X/LCDinit.c ../LCD.X/OLEDinit.c ../LCD.X/LCDpulseEnableBit.c ../LCD.X/LCDputs.c ../LCD.X/LCDletter.c ../LCD.X/LCDcommand.c ../LCD.X/Delay_short.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/2141786296/Delay_ms.o ${OBJECTDIR}/_ext/2141786296/LCDsend.o ${OBJECTDIR}/_ext/2141786296/LCDbusy.o ${OBJECTDIR}/_ext/2141786296/LCDinit.o ${OBJECTDIR}/_ext/2141786296/OLEDinit.o ${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o ${OBJECTDIR}/_ext/2141786296/LCDputs.o ${OBJECTDIR}/_ext/2141786296/LCDletter.o ${OBJECTDIR}/_ext/2141786296/LCDcommand.o ${OBJECTDIR}/_ext/2141786296/Delay_short.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/2141786296/Delay_ms.o.d ${OBJECTDIR}/_ext/2141786296/LCDsend.o.d ${OBJECTDIR}/_ext/2141786296/LCDbusy.o.d ${OBJECTDIR}/_ext/2141786296/LCDinit.o.d ${OBJECTDIR}/_ext/2141786296/OLEDinit.o.d ${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o.d ${OBJECTDIR}/_ext/2141786296/LCDputs.o.d ${OBJECTDIR}/_ext/2141786296/LCDletter.o.d ${OBJECTDIR}/_ext/2141786296/LCDcommand.o.d ${OBJECTDIR}/_ext/2141786296/Delay_short.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/2141786296/Delay_ms.o ${OBJECTDIR}/_ext/2141786296/LCDsend.o ${OBJECTDIR}/_ext/2141786296/LCDbusy.o ${OBJECTDIR}/_ext/2141786296/LCDinit.o ${OBJECTDIR}/_ext/2141786296/OLEDinit.o ${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o ${OBJECTDIR}/_ext/2141786296/LCDputs.o ${OBJECTDIR}/_ext/2141786296/LCDletter.o ${OBJECTDIR}/_ext/2141786296/LCDcommand.o ${OBJECTDIR}/_ext/2141786296/Delay_short.o

# Source Files
SOURCEFILES=../LCD.X/Delay_ms.c ../LCD.X/LCDsend.c ../LCD.X/LCDbusy.c ../LCD.X/LCDinit.c ../LCD.X/OLEDinit.c ../LCD.X/LCDpulseEnableBit.c ../LCD.X/LCDputs.c ../LCD.X/LCDletter.c ../LCD.X/LCDcommand.c ../LCD.X/Delay_short.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib.X.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=33CH128MP506
MP_LINKER_FILE_OPTION=,--script=p33CH128MP506.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/2141786296/Delay_ms.o: ../LCD.X/Delay_ms.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/Delay_ms.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/Delay_ms.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/Delay_ms.c  -o ${OBJECTDIR}/_ext/2141786296/Delay_ms.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/Delay_ms.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/Delay_ms.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDsend.o: ../LCD.X/LCDsend.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDsend.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDsend.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDsend.c  -o ${OBJECTDIR}/_ext/2141786296/LCDsend.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDsend.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDsend.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDbusy.o: ../LCD.X/LCDbusy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDbusy.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDbusy.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDbusy.c  -o ${OBJECTDIR}/_ext/2141786296/LCDbusy.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDbusy.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDbusy.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDinit.o: ../LCD.X/LCDinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDinit.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDinit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDinit.c  -o ${OBJECTDIR}/_ext/2141786296/LCDinit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDinit.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDinit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/OLEDinit.o: ../LCD.X/OLEDinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/OLEDinit.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/OLEDinit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/OLEDinit.c  -o ${OBJECTDIR}/_ext/2141786296/OLEDinit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/OLEDinit.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/OLEDinit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o: ../LCD.X/LCDpulseEnableBit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDpulseEnableBit.c  -o ${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDputs.o: ../LCD.X/LCDputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDputs.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDputs.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDputs.c  -o ${OBJECTDIR}/_ext/2141786296/LCDputs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDputs.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDputs.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDletter.o: ../LCD.X/LCDletter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDletter.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDletter.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDletter.c  -o ${OBJECTDIR}/_ext/2141786296/LCDletter.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDletter.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDletter.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDcommand.o: ../LCD.X/LCDcommand.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDcommand.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDcommand.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDcommand.c  -o ${OBJECTDIR}/_ext/2141786296/LCDcommand.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDcommand.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDcommand.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/Delay_short.o: ../LCD.X/Delay_short.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/Delay_short.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/Delay_short.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/Delay_short.c  -o ${OBJECTDIR}/_ext/2141786296/Delay_short.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/Delay_short.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/Delay_short.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/_ext/2141786296/Delay_ms.o: ../LCD.X/Delay_ms.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/Delay_ms.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/Delay_ms.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/Delay_ms.c  -o ${OBJECTDIR}/_ext/2141786296/Delay_ms.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/Delay_ms.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/Delay_ms.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDsend.o: ../LCD.X/LCDsend.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDsend.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDsend.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDsend.c  -o ${OBJECTDIR}/_ext/2141786296/LCDsend.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDsend.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDsend.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDbusy.o: ../LCD.X/LCDbusy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDbusy.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDbusy.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDbusy.c  -o ${OBJECTDIR}/_ext/2141786296/LCDbusy.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDbusy.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDbusy.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDinit.o: ../LCD.X/LCDinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDinit.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDinit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDinit.c  -o ${OBJECTDIR}/_ext/2141786296/LCDinit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDinit.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDinit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/OLEDinit.o: ../LCD.X/OLEDinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/OLEDinit.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/OLEDinit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/OLEDinit.c  -o ${OBJECTDIR}/_ext/2141786296/OLEDinit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/OLEDinit.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/OLEDinit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o: ../LCD.X/LCDpulseEnableBit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDpulseEnableBit.c  -o ${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDpulseEnableBit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDputs.o: ../LCD.X/LCDputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDputs.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDputs.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDputs.c  -o ${OBJECTDIR}/_ext/2141786296/LCDputs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDputs.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDputs.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDletter.o: ../LCD.X/LCDletter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDletter.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDletter.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDletter.c  -o ${OBJECTDIR}/_ext/2141786296/LCDletter.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDletter.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDletter.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/LCDcommand.o: ../LCD.X/LCDcommand.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDcommand.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/LCDcommand.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/LCDcommand.c  -o ${OBJECTDIR}/_ext/2141786296/LCDcommand.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/LCDcommand.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/LCDcommand.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/2141786296/Delay_short.o: ../LCD.X/Delay_short.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2141786296" 
	@${RM} ${OBJECTDIR}/_ext/2141786296/Delay_short.o.d 
	@${RM} ${OBJECTDIR}/_ext/2141786296/Delay_short.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../LCD.X/Delay_short.c  -o ${OBJECTDIR}/_ext/2141786296/Delay_short.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2141786296/Delay_short.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/2141786296/Delay_short.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: archive
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib.X.${OUTPUT_SUFFIX} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  -omf=elf -r dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib.X.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib.X.${OUTPUT_SUFFIX} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  -omf=elf -r dist/${CND_CONF}/${IMAGE_TYPE}/LCDlib.X.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
